import {Component} from '@angular/core';
import {Http, Headers} from '@angular/http';

@Component({
  template: require('./app.component.pug')(),
  selector: 'app',
})
export class AppComponent {

  // Attributes
  private link: string;
  private fLink: string;
  private selector: string;
  private fSelector: string;

  // Methods
  constructor (http: Http) {
    this.fLink = 'https://audioclub.top';
  }
  public updateLink () {
    this.fLink = this.link;
  }
  public updateSelector () {
    this.fSelector = this.selector;
  }

}