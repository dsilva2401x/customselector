import { Directive, ElementRef, Input } from '@angular/core';

@Directive({ selector: '[highlightByRegex]' })
export class HighlightDirective {
    
  @Input() highlightByRegex: string;
  iframeDocElem: any;

  constructor(el: ElementRef) {
    this.iframeDocElem = el;
  }

  ngOnChanges (changes) {
    var customSelector = changes.highlightByRegex.currentValue;
    var customStyle = customSelector+' {border: 3px solid red;}';
    var elems = this.iframeDocElem.nativeElement.contentDocument.getElementsByClassName('tempclass123');
    while(elems[0]) {
        elems[0].parentNode.removeChild(elems[0]);
    }​
    this.iframeDocElem.nativeElement.contentDocument.body.innerHTML += '<style class="tempclass123">'+customStyle+'</style>';
  }

}